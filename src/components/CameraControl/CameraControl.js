import React from "react";

import "./CameraControl.css";

class CameraControl extends React.Component {
  constructor(props) {
    super(props);

    this.isMoving = false;
    this.firstTouch = false;

    this.webCamControlRef = React.createRef();
  }

  setupDesktop() {
    this.webCamControlRef.current.addEventListener("mousedown", (e) => {
      this.isMoving = true;
    });

    window.addEventListener("mousemove", (e) => {
      this.move(e.movementX, -e.movementY);
    });

    window.addEventListener("mouseup", (e) => {
      this.isMoving = false;
    });
  }

  setupMobile() {
    this.webCamControlRef.current.addEventListener("touchstart", (e) => {
      this.isMoving = true;
      this.firstTouch = true;
    });

    window.addEventListener("touchmove", (e) => {
      if (!this.firstTouch) {
        // We only consider the first touch to be a valid input, as it's not
        // obvious how to interpret multi-touch commands.
        const currentX = e.changedTouches[0].clientX;
        const currentY = e.changedTouches[0].clientY;

        const deltaX = currentX - this.lastX;
        const deltaY = currentY - this.lastY;

        this.move(deltaX, -deltaY);
      }

      this.lastX = e.changedTouches[0].clientX;
      this.lastY = e.changedTouches[0].clientY;
      this.firstTouch = false;
    });

    window.addEventListener("touchend", (e) => {
      this.isMoving = false;
    });
  }

  componentDidMount() {
    this.setupDesktop();
    this.setupMobile();
  }

  move = (x, y) => {
    if (this.isMoving) {
      window.WebCam.move(x, y);
    }
  };

  render() {
    return <div className="CameraControl" ref={this.webCamControlRef}></div>;
  }
}

export default CameraControl;
