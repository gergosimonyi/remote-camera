import React from "react";

import "./CameraListItem.css";

class CameraListItem extends React.Component {
  handleClick = () => {
    window.WebCam.setSource(this.props.source);
  };

  render() {
    return (
      <button className="CameraListItem" onClick={this.handleClick}>
        {this.props.name}
      </button>
    );
  }
}

export default CameraListItem;
