import React from "react";

import Menu from "./components/Menu";
import CameraList from "./components/CameraList";
import CameraControl from "./components/CameraControl";
import CameraView from "./components/CameraView";

import "./App.css";

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      tab: "cameras",
    };
  }

  handleTabChange = (tab) => {
    this.setState({ tab });
  };

  render() {
    const { tab } = this.state;
    const selectedTab = tab === "cameras" ? <CameraList /> : <CameraControl />;

    return (
      <div className="App">
        <h1>EarthCam - use responsibly</h1>

        <div className="App-Content">
          <div className="App-CameraView">
            <CameraView />
          </div>
          <div className="App-Interactable">
            <Menu currentTab={tab} onTabChange={this.handleTabChange} />
            {selectedTab}
          </div>
        </div>
      </div>
    );
  }
}

export default App;
