import React from "react";

class CameraView extends React.Component {
  constructor(props) {
    super(props);
    this.webCamRef = React.createRef();
  }

  componentDidMount() {
    const webCamNode = window.WebCam.getCameraNode();
    this.webCamRef.current.appendChild(webCamNode);
  }

  render() {
    return <div ref={this.webCamRef}></div>;
  }
}

export default CameraView;
