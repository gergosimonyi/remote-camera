import React from "react";

import CameraListItem from "./CameraListItem";

import "./CameraList.css";

class CameraList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      items: [],
    };
  }

  componentDidMount() {
    fetch("https://runningios.com/screamingbox/cameras.json")
      .then((res) => res.json())
      .then(
        (result) => {
          this.setState({
            isLoaded: true,
            items: result,
          });
        },
        (error) => {
          this.setState({
            isLoaded: true,
            error,
          });
        }
      );
  }

  render() {
    const { error, isLoaded, items } = this.state;

    if (error) {
      return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
      return <div>Loading...</div>;
    } else {
      return (
        <div className="CameraList">
          {items.map((item) => (
            <CameraListItem
              key={item.name}
              name={item.name}
              source={item.source}
            />
          ))}
        </div>
      );
    }
  }
}

export default CameraList;
