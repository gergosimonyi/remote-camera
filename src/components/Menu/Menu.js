import React from "react";

import "./Menu.css";

class Menu extends React.Component {
  setCamerasTab = () => {
    this.props.onTabChange("cameras");
  };

  setControlTab = () => {
    this.props.onTabChange("control");
  };

  render() {
    return (
      <div className="Menu">
        <button onClick={this.setCamerasTab}>Sources</button>
        <button onClick={this.setControlTab}>Control</button>
      </div>
    );
  }
}

export default Menu;
